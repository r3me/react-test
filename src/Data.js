export default {
  data: {
    title: 'Category Name',
    items: [
      {
        id: '111',
        seo: {
          name: 'First item',
          url: '/item-1',
          category: 'tech',
          description: 'This is a short description of the item',
          keywords: ['tech', 'sale', 'offer']
        },
        images: [
          'item-image.jpg',
        ],
        pricing: {
          list_price: 230000,
          offer_price: 190000
        },
        attributes: {
          color: 'black',
        },
        available: true,
      },
      {
        id: '222',
        seo: {
          name: 'Second item',
          url: '/item-2',
          category: 'tech',
          description: 'This is a short description of the item',
          keywords: ['tech', 'sale', 'offer']
        },
        images: [
          'item-image.jpg',
        ],
        pricing: {
          list_price: 230000,
          offer_price: 190000
        },
        attributes: {
          color: 'black',
        },
        available: true,
      },
      {
        id: '333',
        seo: {
          name: 'Third item',
          url: '/item-3',
          category: 'tech',
          description: 'This is a short description of the item',
          keywords: ['tech', 'sale', 'offer']
        },
        images: [
          'item-image.jpg',
        ],
        pricing: {
          list_price: 230000,
          offer_price: 190000
        },
        attributes: {
          color: 'black',
        },
        available: true,
      },
      {
        id: '333',
        seo: {
          name: 'Unavailable item',
          url: '/item-4',
          category: 'tech',
          description: 'This is a short description of the item',
          keywords: ['tech', 'sale', 'offer']
        },
        images: [
          'item-image.jpg',
        ],
        pricing: {
          list_price: 230000,
          offer_price: 190000
        },
        attributes: {
          color: 'black',
        },
        available: false,
      },
    ]
  }
}