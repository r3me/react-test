import React from 'react';

import image from '../assets/component-demo.jpg';

export const Test1 = () => (
  <section>
    <p>1.- Create this component using JSX and Scss</p>
    <picture>
      <img src={image} alt="alt" />
    </picture>
  </section>
);