import React from 'react';

export const ItemPage = () => {
  const itemId = '???';
  return(
    <div>
      <h1>Page for clicked item</h1>
      <p>Item ID: {itemId}</p>
    </div>
  );
}